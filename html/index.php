<?php

require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');

include ('inc/header.php');
?>
<div class="bigwrap">
    <img src="asset/image/home/sfr-img-man-home.jpg" alt="Employé">
</div>
<section id="offre">
    <div class="wrap2">
        <div class="offre_text">
            <h2>Les postes par régions</h2>
            <p>Cliquez sur la carte pour découvrir les postes proposés par localisation</p>
        </div>
        <div class="offre_carte">
            <img src="asset/image/home/carte.svg">
        </div>
    </div>
</section>
<section id="profil">
    <div class="wrap2">
        <h2>Les profils</h2>
        <div class="profil_commerciaux profil_display">
            <div class="profil_text profil_top">
                <h3>Commerciaux itinérants h/f</h3>
                <p>Rattaché au Chef des Ventes, vous êtes chargé de proposer aux TPE/PME des solutions voix, data et services de l'opérateur SFR. Vous êtes responsable de l'atteinte de vos objectifs et du développement de votre parc client. Vous gérez la relation avec vos clients : qualification du besoin, élaboration de la proposition commerciale, signature du contrat, phase d'installation.</p>
                <p>De formation commerciale (min. Bac +2), vous possédez une expérience commerciale réussie de 2 ans minimum dans la vente de solutions technologiques auprès d'entreprises, idéalement dans les secteurs des Télécoms de l'Informatique ou de la Bureautique, idéalement en B to B. Vous avez l'esprit d'analyse et de synthèse, un sens de la négociation et relationnel développé. Vous avez de réelles aptitudes pour la recherche de nouveaux marchés sur le tissu économique local de votre secteur.</p>
            </div>
            <div class="profil_img profil_top">
                <img src="asset/image/home/img-profil1.jpg" alt="Commerciaux">
            </div>
        </div>
        <div class="profil_techno profil_display">
            <div class="profil_img profil_bottom">
                <img src="asset/image/home/img-profil2.jpg" alt="Serveur">
            </div>
            <div class="profil_text profil_bottom">
                <h3>Ingénieur technico-commercial h/f</h3>
                <p>Rattaché au DIrecteur Technique, du Chef des Ventes ou du Dirigeant, vous êtes garant de la pertinence de la solution client du point de vue technique et opérationnel.<br>Il réalise les prestations techniques ou intellectuelles liées à la vente de solutions annexes"<br>Vous intervenez en avant-vente : qualification des besoins clients sur le plan technique, étude et argumentaire autour des solutions techniques proposées, soutien du chef de projet déploiement à l'implémentation de la solution, installation technique possible, accompagnement technique pour les Ingénieurs Commerciaux.<br>De formation Ecole d'Igénieur Télécoms ou informatique et Réseaux (entre Bac +2/3 et Bac +5), vous avez des connaissances techniques dans le domaine de la téléphonie Mobile, Fixe et Internet et savez analyser une infrastructure Télécom. Vous évaluez les besoins techniques d'un client et élaborez les solutions adaptées. Vous avez la capacité à gérer des problèmes complexes et apportez une valeur ajoutée technique auprès du client. Interlocuteur crédible et fiable, vous avez le sens du relationnel en interne comme en externe.<br>Une certification consructeur de type Microsoft, Cisco ou Alcatel Lucent et/ ou une expérience comme ITC / Ingénieur réussie chez un IP, un intégrateur réseau voire un hébergeur ou un opérateur télécom seraient un plus.</p>
            </div>
        </div>
    </div>
</section>

<?php
include ('inc/footer.php');
