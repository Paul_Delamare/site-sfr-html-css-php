<?php

require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');

if (!empty($_GET['id']) && is_numeric($_GET['id'])){
    $id = $_GET['id'];
    $message= getMessageById($id);
    if (empty($message)){
        die('404');
    }
}else{
    die('404');
}
debug($message);
include ('inc/header.php');
?>
<div class="bigwrap">
    <img src="asset/image/image-fond.jpg" alt="Image de fond">
</div>
<section id="detail">
    <div class="wrap">
        <?php
        echo '<div class="detail_message">';
        echo '<h1>'.$message['email'].'</h1>';
        echo '<p>'.nl2br($message['message']).'</p>';
        echo '<p class="date">'.date('d/m/Y à H:i:s', strtotime($message['created_at'])).'</p>'
        ?>
    </div>
</section>



<?php
include ('inc/footer.php');

