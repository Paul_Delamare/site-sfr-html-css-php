<?php

function debug($tableau)
{
echo '<pre style="height: 200px; overflow-y: scroll; font-size: .5rem; padding: .6rem; font-family: Consolas, Monospace; background-color: #000; color: #fff;">';
    print_r($tableau);
    echo '</pre>';
}
function cleanXss($key){
    return trim(strip_tags($_POST[$key]));
}
function getPostValue($key){
    if(!empty($_POST[$key]) ) {
        echo $_POST[$key]; }
}
function getMessageById($id){
    global $pdo;
    $sql = "SELECT * FROM contact WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('id', $id, PDO::PARAM_INT);
    $query->execute();
    return $query->fetch();
}
