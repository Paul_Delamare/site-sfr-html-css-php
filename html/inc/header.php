<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice html/css/php</title>
    <link rel="stylesheet" href="asset/css/style.css">
</head>
<body>
<header id="masthead">
    <div class="wrap">
        <div class="header_sfr">
            <a href="index.php">
                <img src="asset/image/header/logo-sfr.svg" alt="Logo SFR">
            </a>
        </div>
        <nav>
            <ul class="header_lien">
                <li class="header_profil navheader">
                    <a href="index.php#profil">
                        <img src="asset/image/header/logo-menu1.svg" alt="Image profil">
                        <p>Les profils</p>
                    </a>
                </li>
                <li class="header_offre navheader">
                    <a href="index.php#offre">
                        <img src="asset/image/header/logo-menu2.svg" alt="Image région">
                        <p>Les offres par région</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</header>

