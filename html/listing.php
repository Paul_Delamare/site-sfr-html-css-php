<?php
require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');

$sql = "SELECT * FROM contact ORDER BY created_at";
$query = $pdo->prepare($sql);
$query->execute();
$messages = $query->fetchAll();
debug($messages);


include ('inc/header.php');
?>
<div class="bigwrap">
    <img src="asset/image/image-fond.jpg" alt="Image de fond">
</div>
<section id="message">
    <div class="wrap">
        <?php  foreach ($messages as $message){
            echo '<div class="listing_message">';
            echo '<h3><a href="single.php?id='.$message['id'].'">'.$message['email'].'</a></h3>';
            echo '</div>';
        } ?>
    </div>
</section>


<?php
include ('inc/footer.php');
