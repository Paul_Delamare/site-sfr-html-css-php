<?php

require ('inc/pdo.php');
require ('inc/function.php');
require ('inc/validation.php');
$errors= array();
$suc=false;

if (!empty($_POST['submit'])){
    $email= cleanXss('email');
    $message= cleanXss('message');
    $errors= validationEmail($errors, $email);
    $errors= validationText($errors, $message, 'message', 5, 2000);
    if (count($errors)==0){
        $sql= "INSERT INTO contact (email, message, created_at) VALUES (:email, :message, NOW())";
        $query = $pdo->prepare($sql);
        $query->bindValue('email', $email);
        $query->bindValue('message', $message);
        $query->execute();
        $suc=true;
    }
}

include ('inc/header.php');
?>
<section id="formulaire">
    <div class="bigwrap">
        <img src="asset/image/image-fond.jpg" alt="Image de fond">
        <div class="bordure">
            <div class="transparent">
                <h2>Les offres par région</h2>
            </div>
        </div>
    </div>

    <div class="wrap3">
        <?php if ($suc==true){ ?>
            <h2>Le message a bien été envoyé !</h2>
        <?php }else{  ?>
            <form method="post" action="" novalidate>
                <div class="contact_email input_css">
                    <input type="email" name="email" placeholder="Votre mail">
                    <span class="error"><?php if (!empty($errors['email'])){ echo $errors['email']; }  ?></span>
                </div>
                <div class="contact_message input_css">
                    <textarea name="message" placeholder="Votre message" id="contact_message"></textarea>
                    <span class="error"><?php if (!empty($errors['message'])){ echo $errors['message']; }  ?></span>
                </div>
                <div class="submit">
                    <input type="submit" name="submit" id="submit" value="Envoyer">
                </div>
            </form>
        <?php } ?>
    </div>
</section>
<?php
include ('inc/footer.php');
