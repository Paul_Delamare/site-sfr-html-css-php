-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 02 nov. 2022 à 13:48
-- Version du serveur : 10.4.25-MariaDB
-- Version de PHP : 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sql_sfr`
--

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `email`, `message`, `created_at`) VALUES
(1, 'paulodela2006@gmail.com', 'C\'est vraiment bien', '2022-11-02 12:35:20'),
(2, 'paulodela2006@gmail.com', 'C\'est vraiment bien', '2022-11-02 12:35:40'),
(3, 'verif@gmail.com', 'czeyncf_enya', '2022-11-02 12:36:04'),
(4, 'verif@gmail.com', 'czeyncf_enya', '2022-11-02 12:37:06'),
(5, 'gegenicodela@gmail.com', 'ucfenacf_\"rac', '2022-11-02 12:38:40'),
(6, 'gegenicodela@gmail.com', 'ucfenacf_\"rac', '2022-11-02 12:39:00'),
(7, 'gegenicodela@gmail.com', 'ucfenacf_\"rac', '2022-11-02 12:39:11'),
(8, 'gegenicodela@gmail.com', 'ucfenacf_\"rac', '2022-11-02 12:39:33'),
(9, 'gegenicodela@gmail.com', 'ucfenacf_\"rac', '2022-11-02 12:40:54'),
(10, 'gegenicodela@gmail.com', 'ucfenacf_\"rac', '2022-11-02 12:41:20'),
(11, 'gegenicodela@gmail.com', 'ucfenacf_\"rac', '2022-11-02 12:41:37'),
(12, 'gegenicodela@gmail.com', 'hezucbnyf_açcx', '2022-11-02 12:41:47'),
(13, 'gegenicodela@gmail.com', 'runcfnyxç xrèa', '2022-11-02 12:46:49'),
(14, 'paulodela2006@gmail.com', 'ucebaf\r\ncfreagbhnf\r\ncbehrqb', '2022-11-02 13:21:49');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
